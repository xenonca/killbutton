unified_inventory.register_button("killme", {
	type = "image",
	image = "killbutton_kill.png",
	tooltip = "Kill me",
	hide_lite=true,
    action = function(player)
        name = player:get_player_name()
        minetest.show_formspec(name, "killbutton:verify",
        "size[4,2]" ..
        "label[0,0;Are you sure you want to kill yourself?]" ..
        "button_exit[0,1;2,1;kill;yes, kill me!]"..
        "button_exit[2,1;2,1;cancel;Nevermind!]"
    )
	end,
	condition = function(player)
		return minetest.check_player_privs(player:get_player_name(), {interact=true})
	end,
})

minetest.register_on_player_receive_fields(function(player, formname, fields)
	if formname == "killbutton:verify" then
        if fields.kill then
            player:set_hp(0)
        end
	end
end)
